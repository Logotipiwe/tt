import React from 'react';
import './App.css';

function App() {
  return (
    <div id="App">
      <header>
        <div id='add'><img src='img/add-user.svg' height={20}/></div>
        <div id='header-username'>Logotipiwe</div>
        <div id='settings'>
          <div></div>
          <div></div>
          <div></div>
        </div>
      </header>
      <main>
        <div id='user-data'>
          <img id='avatar' src='img/ava.jpg'/>
          <div id='main-username'>@Logotipiwe_</div>
          <div id='metrics'>
            <div className='cell'>
              <div>0</div>
              <p>Подписки</p>
            </div>
            <div className='cell'>
              <div>2</div>
              <p>Подписчики</p>
            </div>
            <div className='cell'>
              <div>13</div>
              <p>Лайки</p>
            </div>
          </div>
          <div id='navigation'>
            <div id='profile-edit'>Изменить профиль</div>
            <div id='likes'><img src='img/bookmark.svg'/></div>
          </div>
          <div id='description'>
            <p>Рассказываю о жизни фронтенд разработчика</p>
          </div>
        </div>
        <div id='sections'>
          <div className='section'>|||</div>
          <div className='section'><img src='img/heart.svg'/></div>
          <div className='section'><img src='img/lock.svg'/></div>
        </div>
        <div id='videos-list'>
          <div className='video'/>
          <div className='video'/>
          <div className='video'/>
          <div className='video'/>
          <div className='video'/>
        </div>
      </main>
      <footer>
        <div className='footer-item'>
          <img src='img/home.svg'/>
          <div>Главная</div>
        </div>
        <div className='footer-item'>
          <img src='img/search.svg'/>
          <div>Интересное</div>
        </div>
        <div className='footer-item'>
          <div className='plus'>+</div>
        </div>
        <div className='footer-item'>
          <img src='img/comment.svg'/>
          <div>Входящие</div>
        </div>
        <div className='footer-item'>
          <img src='img/user.svg'/>
          <div>Я</div>
        </div>
      </footer>
    </div>
  );
}

export default App;
